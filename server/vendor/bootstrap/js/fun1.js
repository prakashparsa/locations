           var map,lati,longi,pin="500008";
	       var markersArray = [];
	       function checkWithinBoundary(x,y){
		      draw = new google.maps.Polygon({paths:x});
		      return google.maps.geometry.poly.containsLocation(y, draw);
	       }
	       function convertLatlng(x){
		   var arrs=JSON.parse(x)["coordinates"][0];
			 var objs = arrs.map(function(x) { 
			 return { 
				lat: x[1], 
				lng: x[0] 
			 };
             });
			return objs;
	       }
	       function clearOverlays() {
                for (var i = 0; i < markersArray.length; i++ ) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
        }
           function removeElement(val){
            const index = options.indexOf(val.id);
            var k=document.getElementById(val.id+"1");
            k.remove();
            val.remove();
            options.splice(index,1);
            if(options.length==0){
                $('#it').val('selectVal');
            }
        }